package de.horstblocks.horstpermission.spigot;

import de.horstblocks.horstpermission.core.PermissionService;
import de.horstblocks.horstpermission.core.database.DatabaseHandler;
import de.horstblocks.horstpermission.core.database.mysql.MySQLHandler;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

@Getter
public final class PermissionSpigot extends JavaPlugin implements PermissionService {

    private static PermissionSpigot instance;
    private MySQLHandler mysqlHandler;

    @Override
    public void onEnable() {
        instance = this;

        try {
            this.mysqlHandler = new MySQLHandler();
            getMysqlHandler().connect();
        } catch (ClassAlreadyInitializedException | IOException ex) {
            ex.printStackTrace();
        }

        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public DatabaseHandler getDatabaseHandler() {
        return mysqlHandler;
    }
}
