package de.horstblocks.horstpermission.core.database.mysql;

import de.horstblocks.horstpermission.core.configuration.Configuration;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import lombok.Getter;

import java.io.IOException;

@Getter
public final class MySQLConfig extends Configuration {

    private static MySQLConfig instance;

    private String host = "127.0.0.1";
    private int port = 3306;
    private String database = "database";
    private String username = "root";
    private String password = "1234";
    private int queueCapacity = 100;
    private int corePoolSize = 100;
    private int maximumPoolSize = 100;

    public MySQLConfig(String name, String folderPath) throws IOException, ClassAlreadyInitializedException {
        super(name, folderPath);

        if (instance != null) {
            throw new ClassAlreadyInitializedException(instance.getClass());
        }

        instance = this;

        if (getMeta().isNewest()) {
            putString("host", "127.0.0.1");
            putInteger("port", 3306);
            putString("database", "database");
            putString("username", "root");
            putString("password", "1234");
            putInteger("queueCapacity", 100);
            putInteger("corePoolSize", 100);
            putInteger("maximumPoolSize", 100);
        } else {
            if (!containsKey("host")) { putString("host", "127.0.0.1"); } else { host = getString("host"); }
            if (!containsKey("port")) { putInteger("port", 3306); } else { port = getInteger("port"); }
            if (!containsKey("database")) { putString("database", "database"); } else { database = getString("database"); }
            if (!containsKey("username")) { putString("username", "root"); } else { username = getString("username"); }
            if (!containsKey("password")) { putString("password", "1234"); } else { password = getString("password"); }
            if (!containsKey("queueCapacity")) { putInteger("queueCapacity", 100); } else { queueCapacity = getInteger("queueCapacity"); }
            if (!containsKey("corePoolSize")) { putInteger("corePoolSize", 100); } else { corePoolSize = getInteger("corePoolSize"); }
            if (!containsKey("maximumPoolSize")) { putInteger("maximumPoolSize", 100); } else { maximumPoolSize = getInteger("maximumPoolSize"); }
        }

        saveConfiguration();
    }
}
