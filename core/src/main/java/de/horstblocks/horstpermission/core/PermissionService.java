package de.horstblocks.horstpermission.core;

import de.horstblocks.horstpermission.core.database.DatabaseHandler;

public interface PermissionService {

    static String getPrefix() { return "§8[§cHorstPermission§8] "; }

    DatabaseHandler getDatabaseHandler();

}
