package de.horstblocks.horstpermission.core.thread;

import lombok.Getter;

import java.util.concurrent.ThreadFactory;

@Getter
public class HorstGroupFactory implements ThreadFactory {

    private final ThreadGroup group;
    private String namePrefix;
    private int numThreads;
    private boolean createDaemonThreads;
    private final Object syncLock = new Object();

    public HorstGroupFactory(ThreadGroup group, String namePrefix) {
        this.group = group;
        this.namePrefix = namePrefix;
        this.numThreads = 0;
    }

    public HorstGroupFactory(String namePrefix) {
        this(Thread.currentThread().getThreadGroup(), namePrefix);
    }


    public void createDaemonThreads(boolean daemonThreads) {
        synchronized (syncLock) {
            this.createDaemonThreads = daemonThreads;
        }
    }

    @Override
    public Thread newThread(Runnable r) {
        String name;
        boolean daemon;

        synchronized (syncLock) {
            name = getNamePrefix() + ++numThreads;
            daemon = createDaemonThreads;
        }

        Thread thread = new Thread(group, r, name);
        thread.setDaemon(daemon);

        return thread;
    }


}
