package de.horstblocks.horstpermission.core.database.callback;

import de.horstblocks.horstpermission.core.util.Callback;

public interface SingleResultCallback<Result> extends Callback<Result> {

    @Override
    void onResult(Result result);
}
