package de.horstblocks.horstpermission.core.configuration;


import com.cedarsoftware.util.io.JsonWriter;
import lombok.Getter;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

@Getter
public abstract class Configuration {

    private final ConfigurationMeta meta;
    private JSONObject object;

    public Configuration(String name, String folderPath) throws IOException {
        this.meta = new ConfigurationMeta(name, folderPath);

        if (getMeta().isNewest()) {
            object = new JSONObject();
            saveConfiguration();
        } else {
            JSONParser parser = new JSONParser();
            try {
                object = (JSONObject) parser.parse(new FileReader(folderPath + "/" + name + ".json"));
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String putString(String key, String value) {
        return (String) object.put(key, value);
    }

    public String getString(String key) {
        return (String) get(key);
    }

    public Integer putInteger(String key, Integer value) {
        return (Integer) object.put(key, new Integer(value));
    }

    public Integer getInteger(String key) {
        return Integer.valueOf(get(key).toString());
    }

    public Long putLong(String key, Long lon) {
        return (Long) object.put(key, new Long(lon));
    }

    public Long getLong(String key) {
        return (Long) get(key);
    }

    public Double putDouble(String key, Double doubl) {
        return (Double) object.put(key, new Double(doubl));
    }

    public Double getDouble(String key) {
        return (Double) get(key);
    }

    public Short putShort(String key, Short shor) {
        return (Short) object.put(key, new Short(shor));
    }

    public Short getShort(String key) {
        return (Short) get(key);
    }

    public Boolean putBoolean(String key, boolean bool) {
        return (Boolean) put(key, new Boolean(bool));
    }

    public Boolean getBoolean(String key) {
        return (Boolean) get(key);
    }

    public List<String> putStringList(String key, List<String> value) {
        return (List<String>) object.put(key, value);
    }

    public List<String> getStringList(String key) {
        return (List<String>) get(key);
    }

    public Object put(String key, Object value) {
        return object.put(key, value);
    }

    public Object get(String key) {
        return object.get(key);
    }

    public boolean containsKey(String key) {
        return object.containsKey(key);
    }

    public boolean containsValue(String value) {
        return object.containsValue(value);
    }

    public Object remove(String key) {
        return object.remove(key);
    }

    public void saveConfiguration() throws IOException {
        FileUtils.writeStringToFile(getMeta().getFile(), JsonWriter.formatJson(getObject().toJSONString()));
    }

}
