package de.horstblocks.horstpermission.core.database.result;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class VoidResult implements Result {

    private final boolean successfully;
    private final List<Throwable> throwables;

}
