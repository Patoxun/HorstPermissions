package de.horstblocks.horstpermission.core.util;

public interface Callback<T> {

    void onResult(T result);

}
