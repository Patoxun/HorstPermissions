package de.horstblocks.horstpermission.core.database.mysql;

import de.horstblocks.horstpermission.core.database.DatabaseHandler;
import de.horstblocks.horstpermission.core.database.callback.SingleResultCallback;
import de.horstblocks.horstpermission.core.database.result.VoidResult;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import de.horstblocks.horstpermission.core.thread.HorstThreadPoolExecutor;
import lombok.Getter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Getter
public final class MySQLHandler implements DatabaseHandler {

    private static MySQLHandler instance;
    private final MySQLConfig mysqlConfig;

    private Connection connection;

    private final ThreadPoolExecutor executor;
    private final MySQLDataHandler dataHandler;

    public MySQLHandler() throws IOException, ClassAlreadyInitializedException {
        if (instance != null) {
            throw new ClassAlreadyInitializedException(instance.getClass());
        }

        instance = this;

        mysqlConfig = new MySQLConfig("mysql", getTargetPath());
        this.executor = new HorstThreadPoolExecutor("mysqlDataPool", getMysqlConfig().getCorePoolSize(), getMysqlConfig().getMaximumPoolSize(), 10 * 1000, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(getMysqlConfig().getQueueCapacity()));
        this.dataHandler = new MySQLDataHandler(this);
    }

    @Override
    public void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + getMysqlConfig().getHost() + ":" + getMysqlConfig().getPort() + "/" + getMysqlConfig().getDatabase() + "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8", getMysqlConfig().getUsername(), getMysqlConfig().getPassword());
            System.out.println("Erfolgreich mit der Datenbank verbunden!");

            SingleResultCallback<VoidResult> voidCallback = new SingleResultCallback<VoidResult>() {
                @Override
                public void onResult(VoidResult voidResult) {
                    if (!voidResult.isSuccessfully()) {
                        System.err.println("Es konnte keine Tabelle nicht erstellt werden!");
                    }
                }
            };

            getDataHandler().update("CREATE TABLE IF NOT EXISTS permissions_player(id text, permission text)", voidCallback).start();
            getDataHandler().update("CREATE TABLE IF NOT EXISTS permissions_ranks(id text, name text, parent text)", voidCallback).start();
            getDataHandler().update("CREATE TABLE IF NOT EXISTS permissions_ranks_permissions(id text, name text, permission text)", voidCallback).start();
            getDataHandler().update("CREATE TABLE IF NOT EXISTS permissions_ranks_player(id text, rank text)", voidCallback).start();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public boolean isConnected() {
        return connection != null;
    }

    @Override
    public void disconnect() {
        if (!isConnected()) {
            return;
        }

        try {
            connection.close();
            System.out.println("Die Verbindung mit der Datenbank wurde geschloßen!");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public String getTargetPath() {
        String path = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
        String[] array = path.split("/");
        return path.replaceAll(array[array.length - 1], "");
    }
}
