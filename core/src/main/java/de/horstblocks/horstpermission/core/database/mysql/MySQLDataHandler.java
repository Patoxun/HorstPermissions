package de.horstblocks.horstpermission.core.database.mysql;

import de.horstblocks.horstpermission.core.database.callback.SingleResultCallback;
import de.horstblocks.horstpermission.core.database.mysql.result.QueryResult;
import de.horstblocks.horstpermission.core.database.result.VoidResult;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import lombok.Getter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Getter
public final class MySQLDataHandler {

    private static MySQLDataHandler instance;
    private final MySQLHandler mysqlHandler;

    public MySQLDataHandler(MySQLHandler mysqlHandler) throws ClassAlreadyInitializedException {
        if (instance != null) {
            throw new ClassAlreadyInitializedException(instance.getClass());
        }

        instance = this;
        this.mysqlHandler = mysqlHandler;
    }

    public Thread update(String query, SingleResultCallback<VoidResult> voidCallback) {
        return getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                boolean successfully = true;
                List<Throwable> throwables = new ArrayList<>();

                if (!getMysqlHandler().isConnected()) {
                    throwables.add(new SQLException("No database connection"));
                    successfully = false;
                }

                PreparedStatement statement = null;
                try {
                    statement = getMysqlHandler().getConnection().prepareStatement(query);
                    statement.execute();
                } catch (SQLException ex) {
                    throwables.add(ex);
                } finally {
                    try {
                        statement.close();
                    } catch (SQLException | NullPointerException ex) {
                        throwables.add(ex);
                    }
                }

                voidCallback.onResult(new VoidResult(successfully, throwables));
            }
        });
    }

    public Thread result(String query, SingleResultCallback<QueryResult> queryCallback) {
        return getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                boolean successfully = true;
                List<Throwable> throwables = new ArrayList<>();

                if (!getMysqlHandler().isConnected()) {
                    throwables.add(new SQLException("No database connection"));
                    successfully = false;
                }

                PreparedStatement statement = null;
                ResultSet resultSet = null;
                try {
                    statement = getMysqlHandler().getConnection().prepareStatement(query);
                    resultSet = statement.executeQuery();
                } catch (SQLException ex) {
                    throwables.add(ex);
                }

                queryCallback.onResult(new QueryResult(successfully, throwables, statement, resultSet));
            }
        });
    }

}
